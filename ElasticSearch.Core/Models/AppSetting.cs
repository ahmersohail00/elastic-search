﻿using ElasticSearch.Core.Enums;

namespace ElasticSearch.Core.Models
{
    public interface IAppSetting
    {
        ElasticSearchSetting ElasticSearchSetting { get; set; }
    }

    public class AppSetting : IAppSetting
    {
        public ElasticSearchSetting? ElasticSearchSetting { get; set; }


    }

    public class AWSSetting
    {
        public string? Domain { get; set; }
        public string? MasterUser { get; set; }
        public string? MasterPassword { get; set; }
    }

    public class ElasticSearchSetting
    {
        public ElasticSearchProvider Provider { get; set; }
        public AWSSetting? AWSSetting { get; set; }
    }
}
