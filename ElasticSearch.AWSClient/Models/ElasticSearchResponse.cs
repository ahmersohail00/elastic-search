﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ElasticSearch.AWSClient.Models
{
    public class ElasticSearchResponse
    {
        [JsonPropertyName("took")]
        public int Took { get; set; }

        [JsonPropertyName("timed_out")]
        public bool TimedOut { get; set; }

        [JsonPropertyName("_shards")]
        public Shards Shards { get; set; }

        [JsonPropertyName("hits")]
        public Hit Hits { get; set; }
    }

    public class Shards
    {
        [JsonPropertyName("total")]
        public int? Total { get; set; }

        [JsonPropertyName("successful")]
        public int? Successful { get; set; }

        [JsonPropertyName("skipped")]
        public int? Skipped { get; set; }

        [JsonPropertyName("failed")]
        public int? Failed { get; set; }
    }

    public class Total
    {
        [JsonPropertyName("value")]
        public int? Value { get; set; }

        [JsonPropertyName("relation")]
        public string Relation { get; set; }
    }

    public class SearchResponse
    {
        public int? Total { get; set; }
        public List<ResponseData> Data { get; set; }
    }

    public class ResponseData
    {

        [JsonPropertyName("_index")]
        public string Index { get; set; }

        [JsonPropertyName("_type")]
        public string Type { get; set; }

        [JsonPropertyName("_id")]
        public string Id { get; set; }

        [JsonPropertyName("_score")]
        public double? Score { get; set; }

        [JsonPropertyName("_source")]
        public object Source { get; set; }
    }

    public class Hit
    {
        [JsonPropertyName("total")]
        public Total Total { get; set; }

        [JsonPropertyName("max_score")]
        public double? MaxScore { get; set; }

        [JsonPropertyName("hits")]
        public List<ResponseData> Hits { get; set; }
    }
}
