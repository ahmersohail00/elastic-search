﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ElasticSearch.AWSClient.Models
{
    internal class ElasticSearchRequest
    {
        [AliasAs("q")]
        public string? Query { get; set; }

        [AliasAs("pretty")]
        public object Pretty { get; set; } = true.ToString().ToLower();


        [AliasAs("from")]
        public int From { get; set; } = 0;

        [AliasAs("size")]
        public int Size { get; set; } = 100;
    }
}
