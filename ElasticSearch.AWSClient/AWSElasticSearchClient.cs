﻿using ElasticSearch.AWSClient.Models;
using ElasticSearch.Core.Models;
using Refit;
using System.Text;

namespace ElasticSearch.AWSClient
{
    public class AWSElasticSearchClient
    {
        private readonly IElasticSearchClient _client;
        private readonly IAppSetting _appSetting;

        public AWSElasticSearchClient(IAppSetting appSetting)
        {
            _appSetting = appSetting;
            if (_appSetting is null || appSetting.ElasticSearchSetting is null || appSetting.ElasticSearchSetting.AWSSetting is null)
                throw new ArgumentNullException(nameof(appSetting));
            _client = RestService.For<IElasticSearchClient>(appSetting?.ElasticSearchSetting?.AWSSetting?.Domain ?? "", new RefitSettings
            {
                AuthorizationHeaderValueGetter = GetToken
            });
        }

        public async Task<SearchResponse> SearchDataAsync(string query, int pageNo, int pageSize)
        {
            try
            {
                var response = await _client.Search(new ElasticSearchRequest { Query = query, From = pageSize * (pageNo - 1), Size = pageSize });
                return new SearchResponse
                {
                    Data = response.Hits.Hits,
                    Total = response.Hits.Total.Value
                };
            }
            catch (ApiException ex)
            {

                throw new Exception(await ex.GetContentAsAsync<string>(), ex);
            }
        }

        private Task<string> GetToken() => Task.FromResult(Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{_appSetting.ElasticSearchSetting?.AWSSetting?.MasterUser}:{_appSetting.ElasticSearchSetting?.AWSSetting?.MasterPassword}")));
    }
}
