﻿using ElasticSearch.AWSClient.Models;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.AWSClient
{
    [Headers("Authorization: Basic")]
    internal interface IElasticSearchClient
    {
        [Get("/_search")]
        Task<ElasticSearchResponse> Search([Query] ElasticSearchRequest request);
    }
}
