using ElasticSearch.AWSClient;
using ElasticSearch.Core.Models;
using ElasticSearch.Service.ElasticSearch.Factory;
using ElasticSearch.Service.ElasticSearch.Factory.Interfaces;
using ElasticSearch.Service.ElasticSearch.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddSingleton<IAppSetting, AppSetting>(opt => builder.Configuration.Get<AppSetting>());

builder.Services.AddSingleton<AWSElasticSearchClient>();

builder.Services.AddControllersWithViews();

builder.Services.Scan(scan => scan
.FromAssemblyOf<IElasticSearchService>()
            .AddClasses(classes => classes.AssignableTo<IElasticSearchService>())
            .AsImplementedInterfaces()
            .WithSingletonLifetime());

builder.Services.AddSingleton<IElasticSearchFactory, ElasticSearchFactory>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSwagger();
app.UseRouting();
app.UseSwaggerUI();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html"); ;

app.Run();
