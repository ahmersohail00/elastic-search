namespace ElasticSearch
{
    public class ElasticSearchResponse
    {
        public string Index { get; set; }

        public string Type { get; set; }

        public string Id { get; set; }

        public double Score { get; set; }

        public object Source { get; set; }
    }

    public class Pagination
    {
        public int PageCount { get; set; }
        public int TotalItemCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }
        public int FirstItemOnPage { get; set; }
        public int LastItemOnPage { get; set; }
    }

    public class BaseResponse<T>
    {
        public Pagination Pagination { get; set; }
        public T Data { get; set; }
    }
}