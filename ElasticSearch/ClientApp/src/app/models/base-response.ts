import { Pagination } from "./pagination";

export interface BaseResponse<T>{
    data: T;
    pagination: Pagination
  }