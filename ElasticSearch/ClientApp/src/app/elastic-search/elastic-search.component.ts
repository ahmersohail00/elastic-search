
import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, map } from 'rxjs/operators';
import { BaseResponse } from '../models/base-response';
import { Pagination } from '../models/pagination';

@Component({
  selector: 'app-elastic-search',
  templateUrl: './elastic-search.component.html',
  styleUrls: ['./elastic-search.component.css']
})
export class ElasticSearchComponent implements OnInit {
  searchData$: Observable<SearchData[]> = of([]);
  searchString: BehaviorSubject<string> = new BehaviorSubject<string>('');
  query: string = '';
  isLoading = false;
  pagination: Pagination = {
    firstItemOnPage: 0,
    hasNextPage: false,
    hasPreviousPage: false,
    isFirstPage: true,
    isLastPage: false,
    lastItemOnPage: 0,
    pageCount: 0,
    pageNumber: 1,
    pageSize:10,
    totalItemCount: 0
  };
  pages: number[] = []

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
  }

  ngOnInit(): void {
    this.searchString.pipe(debounceTime(500), distinctUntilChanged()).subscribe(()=>{
      this.loadData();
    })
  }

  nextPage(){
    this.pagination.pageNumber++;
    this.loadData();
  }

  prevPage(){
    this.pagination.pageNumber--;
    this.loadData();
  }

  gotoPage(page: number){
    this.pagination.pageNumber= page;
    this.loadData();
  }

  searchData(event: any){
    this.query = event.target.value;
    this.pagination.pageNumber = 1;
this.searchString.next(this.query);
  }

  loadData() {
    if(this.query){
      this.isLoading = true;
      var params = {
        query: this.query,
        pageNo: this.pagination.pageNumber,
        pageSize: this.pagination.pageSize
      }
      this.searchData$ = this.http.get<BaseResponse<ElasticSearch[]>>(this.baseUrl + 'elasticsearch', { params }).pipe(map(data => {
        this.pagination = data.pagination;
        this.pages = this.GetPages();
        return data.data.map(d => {
          const name = d.source['name'];
          const market = d.source['market'];
          const state = d.source['state'];
          const res: SearchData = {
            id: d.id,
            type: d.index,
            market,
            state,
            name
          };
          return res;
        })
      }),
        finalize(() => {
          this.isLoading = false;
        })
      );
    }
  }

  private GetPages() : number[]{
    let startPage: number;
    let endPage: number;
    const totalPages = this.pagination.pageCount;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (this.pagination.pageNumber <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (this.pagination.pageNumber + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = this.pagination.pageNumber - 5;
        endPage = this.pagination.pageNumber + 4;
      }
    }
    return Array.from(Array(endPage + 1 - startPage).keys()).map((i) => startPage + i);
  }
}

interface ElasticSearch {
  index: string;
  type: string;
  id: string;
  score: number;
  source: { [index: string]: any };
}

interface SearchData {
  id: string;
  name: string;
  market: string;
  state: string;
  type: string;
}

