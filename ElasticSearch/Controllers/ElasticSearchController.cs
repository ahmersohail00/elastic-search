﻿using ElasticSearch.Core.Models;
using ElasticSearch.Service.ElasticSearch.Factory.Interfaces;
using ElasticSearch.Service.ElasticSearch.Interfaces;
using ElasticSearch.Service.ElasticSearch.Models;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace ElasticSearch.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ElasticSearchController : ControllerBase
    {

        private readonly ILogger<ElasticSearchController> _logger;
        private readonly IElasticSearchService _elasticSearchService;

        public ElasticSearchController(ILogger<ElasticSearchController> logger, IElasticSearchFactory elasticSearchFactory, IAppSetting appSetting)
        {
            _logger = logger;
            _elasticSearchService = elasticSearchFactory.Create(appSetting.ElasticSearchSetting.Provider);
        }

        [HttpGet]
        [ProducesResponseType(typeof(BaseResponse<List<ElasticSearchResponse>>), 200)]
        public async Task<BaseResponse<List<ElasticSearchResponse>>> Get([FromQuery] string query, [FromQuery] int pageNo, [FromQuery] int pageSize)
        {
            _logger.LogInformation("Getting Data from Service");
            var response = await _elasticSearchService.Search(query, pageNo, pageSize);
            var responseData = new StaticPagedList<SearchDataResponse>(response.Data, pageNo, pageSize, response.Total);
            var baseResponse = new BaseResponse<List<ElasticSearchResponse>>
            {
                Data = response.Data.Select(d => new ElasticSearchResponse
                {
                    Id = d.Id,
                    Index = d.Index,
                    Score = d.Score,
                    Source = d.Source,
                    Type = d.Type
                }).ToList(),
                Pagination = new Pagination
                {
                    FirstItemOnPage = responseData.FirstItemOnPage,
                    HasNextPage = responseData.HasNextPage,
                    HasPreviousPage = responseData.HasPreviousPage,
                    IsFirstPage = responseData.IsFirstPage,
                    IsLastPage = responseData.IsLastPage,
                    LastItemOnPage = responseData.LastItemOnPage,
                    PageCount = responseData.PageCount,
                    PageNumber = responseData.PageNumber,
                    PageSize = responseData.PageSize,
                    TotalItemCount = responseData.TotalItemCount,
                }
            };
            return baseResponse;
        }
    }
}