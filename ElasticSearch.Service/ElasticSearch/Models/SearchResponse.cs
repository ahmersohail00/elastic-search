﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Service.ElasticSearch.Models
{
    public class SearchDataResponse
    {
        public string Index { get; set; }

        public string Type { get; set; }

        public string Id { get; set; }

        public double Score { get; set; }

        public object Source { get; set; }
    }

    public class SearchResponse
    {
        public int Total { get; set; }
        public List<SearchDataResponse> Data { get; set; }
    }
}
