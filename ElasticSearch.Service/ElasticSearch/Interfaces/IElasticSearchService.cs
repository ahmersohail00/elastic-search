﻿using ElasticSearch.Core.Enums;
using ElasticSearch.Service.ElasticSearch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Service.ElasticSearch.Interfaces
{
    public interface IElasticSearchService
    {
        Task<SearchResponse> Search(string query, int pageNo = 1, int pageSize = 10);
        ElasticSearchProvider Provider { get; }
    }
}
