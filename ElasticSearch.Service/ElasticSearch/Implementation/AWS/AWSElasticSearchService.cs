﻿using ElasticSearch.Service.ElasticSearch.Interfaces;
using ElasticSearch.Core.Enums;
using ElasticSearch.AWSClient;
using ElasticSearch.Service.ElasticSearch.Models;
using AutoMapper;

namespace ElasticSearch.Service.ElasticSearch.Implementation.AWS
{
    internal class AWSElasticSearchService : IElasticSearchService
    {
        private readonly AWSElasticSearchClient _cleint;
        private readonly IMapper _mapper;

        public AWSElasticSearchService(AWSElasticSearchClient cleint, IMapper mapper)
        {
            _cleint = cleint;
            _mapper = mapper;
        }

        public ElasticSearchProvider Provider => ElasticSearchProvider.AWS;

        public async Task<SearchResponse> Search(string query, int pageNo, int pageSize)
        {
            var response  = await _cleint.SearchDataAsync(query, pageNo, pageSize);

            return _mapper.Map<SearchResponse>(response);
            
        }

    }
}
