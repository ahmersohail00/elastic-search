﻿using AutoMapper;
using ElasticSearch.AWSClient.Models;
using ElasticSearch.Service.ElasticSearch.Models;

namespace ElasticSearch.Service.ElasticSearch.AutoMapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<ResponseData, SearchDataResponse>();
            CreateMap<AWSClient.Models.SearchResponse, Models.SearchResponse>();
        }
    }
}
