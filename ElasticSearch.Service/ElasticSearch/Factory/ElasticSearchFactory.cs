﻿using ElasticSearch.Service.ElasticSearch.Factory.Interfaces;
using ElasticSearch.Service.ElasticSearch.Interfaces;
using ElasticSearch.Core.Enums;

namespace ElasticSearch.Service.ElasticSearch.Factory
{
    public class ElasticSearchFactory : IElasticSearchFactory
    {
        private readonly IEnumerable<IElasticSearchService> _searchServices;

        public ElasticSearchFactory(IEnumerable<IElasticSearchService> searchServices)
        {
            _searchServices = searchServices;
        }

        public IElasticSearchService Create(ElasticSearchProvider provider)
        {
            return _searchServices.Single(s => s.Provider == provider);
        }
    }
}
