﻿using ElasticSearch.Service.ElasticSearch.Interfaces;
using ElasticSearch.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Service.ElasticSearch.Factory.Interfaces
{
    public interface IElasticSearchFactory
    {
        IElasticSearchService Create(ElasticSearchProvider provider);
    }
}
